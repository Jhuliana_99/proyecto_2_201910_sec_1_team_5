package model.data_structures;

import java.util.Iterator;

import controller.Controller;
import junit.framework.TestCase;
import model.data_structures.BusquedaSecuencialST;
import model.data_structures.SeparateChainingHash;
import model.vo.VOMovingViolations;

public class TablaTest extends TestCase{

	SeparateChainingHash<Integer,Integer> tabla;
	
	public void setupScenario1()
	{
		tabla = new	SeparateChainingHash<Integer,Integer>(10);
		
		tabla.put(394, 804);
		tabla.put(554, 94);
		tabla.put(904, 24);
		tabla.put(432, 34);
		tabla.put(786, 242);
		tabla.put(246, 2434);
		tabla.put(390, 2387);
		tabla.put(731, 2564);
		tabla.put(708, 223);
		tabla.put(201, 2367);

	}
	
	
	public void testPut()
	{
		setupScenario1();
		assertTrue("El tama�o de la cola aument� en 1",tabla.getAux() == 10);
	}
	
	public void testGet()
	{
		setupScenario1();
		assertNull("El elemento no deber�a ser encontrado pues no existe",tabla.get(456778));
		
		assertEquals("Tiene que devolver el value de la llave buscada",2387,tabla.get(390));
	}
	
	public void testDelete()
	{
		setupScenario1();
		tabla.delete(708);
		
		assertNull("El elemento no deber�a ser encontrado pues se elimin�",tabla.get(708));
		assertEquals("El tama�o de la cola disminuy� en 1",9,tabla.getAux() );

		
				
	}
}