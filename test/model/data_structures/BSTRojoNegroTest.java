package model.data_structures;
import junit.framework.TestCase;

public class BSTRojoNegroTest extends TestCase{

<<<<<<< HEAD
	private BSTRojoNegro<Integer, String> BST = new BSTRojoNegro<Integer, String>();
=======
	private BSTRojoNegro<Integer, String> BST = new  BSTRojoNegro<Integer, String>();
>>>>>>> 13d51748df614bb37ffc384e40a4b70a4ce89eca

	public void setupScenario1()
	{
		for (int i = 0; i < 50; i++){
			BST.put(i+1, "Val" + String.valueOf(i));
		}
	}

	public void testPut()
	{
		setupScenario1();
		assertFalse("La lista no debe estar vac�a", BST.estaVacio());
		assertEquals("El tama�o de la lista debe ser igual al n�mero de nodos agregados", 50, BST.size());
	}

	public void testEliminar()
	{
		setupScenario1();
		int random = (int)(Math.random() * 50 + 1);
		try {
			BST.eliminar(random);
			assertEquals("El tama�o del arbol debe ser igual al n�mero de nodos agregados menos los eliminados", 49, BST.size());
			assertNull("El elemento no deber�a existir en el arbol", BST.get(random));

		} catch (Exception e) {
			fail("No debe generar excepci�n");
		}

		try{
			BST.eliminar(random);
		}
		catch (Exception e) {
			//Debe generar excepci�n, la llave no existe.
		}

		try
		{
			BST.eliminar(null);
		}
		catch(IllegalArgumentException e)
		{
			//Debe generar IllegalArgumentException ya que el argumento es nulo.
		} catch (Exception e) {

			fail("Se espera que la excepci�n no sea de este tipo");
		}
	}

	public void testEliminarMax(){

		try{
			BST.eliminarMax();
		}
		catch(Exception e)
		{
			//Debe lanzar excepci�n, el arbol est� vac�o.
		}
		setupScenario1();

		try {
			BST.eliminarMax();
			assertNull("El elemento no deber�a existir en el arbol", BST.get(50));
			assertEquals("El tama�o de la lista debe ser igual al n�mero de nodos agregados menos el m�ximo", 49, BST.size());

			BST.put(1200, "val"+ 1200);
			assertNotNull("El elemento fue agregado al arbol", BST.get(1200));
			BST.eliminarMax();
			assertNull("El elemento no deber�a existir en el arbol", BST.get(1200));
			assertEquals("El tama�o de la lista debe ser igual al n�mero de nodos agregados menos el m�ximo", 49, BST.size());

		} 
		catch (Exception e){
			fail("No deber�a lanzar excepci�n");
		}
	}



	public void testEliminarMin(){
		try{
			BST.eliminarMin();
		}
		catch(Exception e)
		{
			//Debe lanzar excepci�n, el arbol est� vac�o.
		}
		setupScenario1();

		try {
			BST.eliminarMin();
			assertNull("El elemento no deber�a existir en el arbol", BST.get(1));
			assertEquals("El tama�o de la lista debe ser igual al n�mero de nodos agregados menos el m�nimo", 49, BST.size());

			BST.put(-1200, "val"+ 120);
			assertNotNull("El elemento fue agregado al arbol", BST.get(-1200));
			BST.eliminarMin();
			assertNull("El elemento no deber�a existir en el arbol", BST.get(-1200));
			assertEquals("El tama�o de la lista debe ser igual al n�mero de nodos agregados menos el m�nimo", 49, BST.size());

		} 
		catch (Exception e){
			fail("No deber�a lanzar excepci�n");
		}
	}

}
