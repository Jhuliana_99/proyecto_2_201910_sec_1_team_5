package model.data_structures;

import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

import controller.Controller;
import junit.framework.TestCase;

public class PruebaColaPrioridad<T> extends TestCase{


	private MaxHeapCP heap;

	private Controller control;

	public void setupScenario1()
	{
<<<<<<< HEAD
		
		cola = new MaxColaPrioridad<>(20);
		heap = new MaxHeapCP<String>(20, null);
		
=======

		heap = new MaxHeapCP<Integer>(10, null);

	}

	public void setupScenario2()
	{
		heap.agregar(50);
		heap.agregar(5);
		heap.agregar(7);
		heap.agregar(89);
		heap.agregar(32);
		heap.agregar(45);
		heap.agregar(12);
		heap.agregar(10);
		heap.agregar(34);
		heap.agregar(52);



>>>>>>> 13d51748df614bb37ffc384e40a4b70a4ce89eca
	}

	public void testDarNumeroElementos()
	{
		setupScenario1();
<<<<<<< HEAD
		
		assertEquals("La cola no tiene la canidad correcta de elementos", 0, cola.darNumElementos());
		assertEquals("El Heap no tiene la canidad correcta de elementos", 0, heap.darNumElementos());
		
		
=======

		assertTrue("El tama�o de la cola aument� en 1",heap.darNumElementos() == 10);
>>>>>>> 13d51748df614bb37ffc384e40a4b70a4ce89eca
	}


	public void testAgregar()
	{
<<<<<<< HEAD
		
		setupScenario1();
		
		String elemento = "1";
		cola.agregar(elemento);
		heap.agregar(elemento);
		
		assertEquals("La cola no agrego el elemento", 1, cola.darNumElementos());
		assertEquals("La heap no agrego el elemento", 1, heap.darNumElementos() );

		
	}
	
	public void testDelMax()
	{
		
		setupScenario1();
		
		assertNull("No deberia retornar nada, cola", cola.delMax());
		assertNull("No deberia retornar nada, heap", heap.delMax());
		
		
=======

		setupScenario1();

		int sizeanterior = heap.darNumElementos();
		heap.agregar(1234);

		assertTrue("El tama�o de la cola aument� en 1",heap.darNumElementos()-1 ==sizeanterior);
>>>>>>> 13d51748df614bb37ffc384e40a4b70a4ce89eca
	}

	/**
	 * Verifica que un elemento aleatorio haya sido agregado en el orden correcto en el heap.
	 */
	public void testAgregarAleatorioHeap()
	{
<<<<<<< HEAD
		
		setupScenario1();
		
		assertNull("No deberia retornar nada, cola", cola.delMax());
		assertNull("No deberia retornar nada, heap", heap.delMax());
		
		
=======
		setupScenario2();

		int ActSize = heap.darNumElementos();

		int x = ThreadLocalRandom.current().nextInt(462, 765);

		heap.agregar(x);

		assertEquals("El tama�o del heap debi� aumentar en 1", ActSize +1, heap.darNumElementos());

		Comparable<Integer>[] arreglo = heap.darArreglo();
		boolean t = false; 
		int index = -1;
		for(int i=1; i < heap.darNumElementos() && t== false ;i++)
		{
			if((Integer)arreglo[i] == x) t = true; index = i;
		}

		assertTrue("El elemento debi� agregarse", index != -1);

		if(arreglo[index/2] != null)
			assertTrue("El elemento agregado debe ser menor a su padre", x <= (Integer)arreglo[index/2]);
		if(arreglo[2*index+1] != null)
			assertTrue("El elemento agregado debe ser mayor que sus hijos", x >= (Integer)arreglo[2*index +1]);
		if(arreglo[2*index] != null)
			assertTrue("El elemento agregado debe ser mayor que sus hijos", x >= (Integer)arreglo[2*index]);
>>>>>>> 13d51748df614bb37ffc384e40a4b70a4ce89eca
	}


	/**
	 * Verifica que un nuevo elemento m�ximo haya sido agregado en el orden correcto al heap
	 */
	public void testAgregarMayorHeap()
	{
<<<<<<< HEAD
		
		setupScenario1();
		
		assertTrue("No deberia retornar nada, cola", cola.esVacia());
		assertTrue("No deberia retornar nada, heap", heap.esVacia());
		
		
=======
		setupScenario2();
		int ActSize = heap.darNumElementos();

		Integer max = (Integer)heap.max();		
		Integer newMaxElement = 67890;

		heap.agregar(newMaxElement);

		assertEquals("El tama�o del heap debi� aumentar en 1", ActSize +1, heap.darNumElementos());
		Comparable[] arreglo = heap.darArreglo();
		assertEquals("El nuevo elemento m�ximo debe ser el agregado", max, arreglo[1]);
		assertTrue("El anterior elemento m�ximo debe ser uno de los hijos del nuevo", (arreglo[2]).equals(max) || (arreglo[3]).equals(max)  );

>>>>>>> 13d51748df614bb37ffc384e40a4b70a4ce89eca
	}

}
