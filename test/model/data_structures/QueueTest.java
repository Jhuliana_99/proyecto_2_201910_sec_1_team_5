package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.TestCase;

public class QueueTest extends TestCase{

		/**
		 * Es la clase donde se har�n las pruebas
		 */
		private Queue<String> colaPrueba;


		// -----------------------------------------------------------------
		// M�todos
		// -----------------------------------------------------------------

		/**
		 * Construye una nueva cola
		 */
		private void setupEscenario1( )
		{
			colaPrueba = new Queue<String>();

			colaPrueba.enqueue("str1");
			colaPrueba.enqueue("str2");
			colaPrueba.enqueue("str3");
			colaPrueba.enqueue("str4");
			colaPrueba.enqueue("str5");
			colaPrueba.enqueue("str6");
			colaPrueba.enqueue("str7");
			colaPrueba.enqueue("str8");
			colaPrueba.enqueue("str9");


		}

		/**
		 * Prueba el m�todo enqueue
		 */
		public void testEnqueue() {
			setupEscenario1();

			int size1 = colaPrueba.size();

			assertEquals("El tama�o de la cola debe ser 9", 9, size1);	

			String str10 = "str10";
			colaPrueba.enqueue(str10);

			boolean found = false;
			Iterator<String> iter = colaPrueba.iterator();
			ArrayList<String> g = new ArrayList<String>();

			while(iter.hasNext())
			{
				g.add(iter.next());
			}

			for(int i= 0; i < g.size(); i++ )
			{
				if(g.get(i).equals(str10))
					found = true;
			}

			assertTrue( "No se encontr� el nuevo elemento agregado", found );
			assertEquals("El �ltimo elemento de la lista debe ser el agregado", g.get(g.size()-1), str10);
			assertEquals( "El n�mero de elementos no es el correcto", size1 + 1, g.size( ) );

		}


		/**
		 * Prueba el m�todo dequeue
		 */
		public void testDequeue()
		{
			setupEscenario1();

			int size1 = colaPrueba.size();

			assertEquals("El tama�o de la cola debe ser 9", 9, size1);	

			colaPrueba.dequeue();

			boolean found = false;
			Iterator<String> iter = colaPrueba.iterator();
			ArrayList<String> g = new ArrayList<String>();

			while(iter.hasNext())
			{
				g.add(iter.next());
			}

			for(int i= 0; i < g.size(); i++ )
			{
				if(g.get(i).equals("str1"))
					found = true;
			}

			assertTrue( "Se encontr� el nuevo elemento agregado", !found );
			assertEquals("El nuevo primer elemento de la lista debe ser el segundo", g.get(0), "str2");
			assertEquals( "El n�mero de elementos no es el correcto", size1 - 1, g.size( ) );
		}



}
