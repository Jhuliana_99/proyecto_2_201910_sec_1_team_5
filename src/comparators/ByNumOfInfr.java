package comparators;

import java.util.Comparator;

import model.vo.EstadisticaInfracciones;
import model.vo.VOMovingViolations;

public class ByNumOfInfr  implements Comparator<EstadisticaInfracciones>{
		@Override
		public int compare(EstadisticaInfracciones arg0, EstadisticaInfracciones arg1) {

			double total2 = arg1.getTotalInfracciones();
			double total1 = arg0.getTotalInfracciones();

			if(total1 == total2) return 0;
			else if(total1>total2) return 1;
			else return -1;
		}
	}
