package model.vo;

//Clase que representa una dupla
public class dupla<K,V> implements Comparable<dupla<K,V>> {

	//Primer elemento de la dupla
	private K kk;

	//Segundo elemento de la dupla
	private V vv;

	//Constructor
	public dupla(K k,V v)
	{
		kk =k;
		vv=v;
	}

	//Retorna el primer elemento de la dupla
	public K getFirst()
	{
		return kk;
	}

	//Retorna el segundo elemento de la dupla
	public V getSecond()
	{
		return vv;
	}

	/**
	 * Solo indica si las dos duplas son iguales, en caso contrario retorna -1 :))
	 */
	public int compareTo(dupla<K, V> g) {

		if((g.getFirst()).equals(this.getFirst()) && (g.getSecond()).equals(this.getSecond()) )
			return 0;
		else
			return -1;
	}

	public String toString()
	{
		return "x: " + getFirst() + ", y: "+ getSecond();
	}
}
