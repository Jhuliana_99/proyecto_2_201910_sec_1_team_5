package model.data_structures;

public interface IHeap<T extends Comparable <? super T>> {

	/**
	 * Retorna el tama�o de la estructura de datos
	 * @return el n�mero de elementos en la estructura de datos
	 */
	public int darNumElementos();	

	/**
	 * Inserta un elemento de acuerdo al orden correspondiente.
	 * @param elemento, es el elemento a insertar
	 */
	public void agregar(T elemento);

	/**
	 * Elimina el elemento mayor de la estructura de datos, es decir, la ra�z del binary heap.
	 */
	public T delMax();

	/**
	 * Coloca el elemento con el �ndice dado en su lugar correspondiente en la estructura
	 * Verifica de abajo hacia arriba
	 * @param k, �ndice del elemento.
	 */
	public void swim(int k);

	/**
	 * Coloca el elemento con el �ndice dado en su lugar correspondiente en la estructura
	 * Verifica de arriba hacia abajo
	 * @param k, �ndice del elemento.
	 */
	public void sink(int k);

	/**
	 * Indica si la lista est� vac�a  
	 * @return true si est� vac�a, false en caso contrario.
	 */
	public boolean esVacia();

}
