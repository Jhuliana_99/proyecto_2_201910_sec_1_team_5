package model.data_structures;

import java.util.Iterator;

public interface IPriorityQueue <T extends Comparable<T>> extends Iterable<T>{
	
	/**
	 * 
	 * @return
	 */
	int darNumElementos();

	/**
	 * 
	 * @param elemento
	 */
	void agregar(Comparable elemento);

	/**
	 * 
	 * @return
	 */
	Comparable delMax();

	/**
	 * 
	 * @return
	 */
	boolean esVacia();

	/**
	 * 
	 * @param i
	 * @param j
	 * @return
	 */
	boolean less(int i, int j);
}
