package model.data_structures;

import java.util.Comparator;

public class MaxHeapCP<T extends Comparable<T>> implements IHeap<T>{

	/**
	 * Tama�o de la lista
	 */
	private int size;

	/**
	 * Arreglo que contiene los elementos de la lista.
	 */
	private Comparable<T>[] arreglo;

	/**
	 * Criterio de comparaci�n del heap de prioridad
	 */
	private Comparator criterioDeComparacion; 


	//Constructor
	public MaxHeapCP(int tama�o, Comparator criterio)
	{
		size = 0;
		arreglo = new Comparable[tama�o+1];
		criterioDeComparacion = criterio;
	}



	public int darNumElementos() {
		return size;
	}

	@Override
	public void agregar(T elemento) {//si el tama�o es 8 el indice maximo es 7
		if(size == arreglo.length-1)
		{
			arreglo[size] = null; 
			arreglo[size] = (Comparable<T>)elemento;
			swim(size);
		}
	else{
		arreglo[++size] = (Comparable<T>)elemento;
		swim(size);
	}
	}

	@Override
	public T delMax() {

		if (esVacia()) return null;
		T max = (T) arreglo[1];
		exchange(1, size--);
		sink(1);
		arreglo[size+1] = null;     
		return max;
	}

	@Override
	public void swim(int k) {
		while (k > 1 && less(k/2, k)) {
			exchange(k, k/2);
			k = k/2;
		}	}

	@Override
	public void sink(int k) {
		while (2*k <= size) {
			int j = 2*k;
			if (j < size && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exchange(k, j);
			k = j;
		}
	}

	@Override
	public boolean esVacia() {

		return size == 0;
	}

	/**
	 * Intercambia dos elementos dadas sus posiciones.
	 * @param i, posici�n del primer elemento a intercambiar.
	 * @param j, posici�n del segundo elemento a intercambiar.
	 */
	public void exchange(int i, int j)
	{
		Comparable t = arreglo[i]; arreglo[i] = arreglo[j]; arreglo[j] = t; 
	}

	/**
	 * Indica si el elemento en el primer �ndice dado es menor que el elemento del segundo �ndice.
	 * @param i �ndice del primer elemento a comparar.
	 * @param j �ndice del segundo elemento a comparar.
	 * @return true si es menor, false en caso contrario.
	 */
	public boolean less(int i, int j)
	{
		 if (criterioDeComparacion == null) {
	            return ((Comparable<T>) arreglo[i]).compareTo((T) arreglo[j]) < 0;
	        }
	        else {
	            return criterioDeComparacion.compare(arreglo[i], arreglo[j]) < 0;
	        }

}



	public Comparable<T>[] darArreglo() {
		return arreglo;
	}



	public T max() {
		return (T)arreglo[1];
	}
}
