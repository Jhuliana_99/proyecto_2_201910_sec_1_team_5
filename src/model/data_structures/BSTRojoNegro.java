package model.data_structures;

import java.util.Iterator;

public class BSTRojoNegro<K extends Comparable<K>, V> implements IBSTRojoNegro<K,V>
{
	//	CONSTANTES

	private static final boolean RED = true;

	private static final boolean BLACK = false;


	//Atributos
	/**
	 * Raiz del arbol
	 */
	private Node raiz;



	/**
	 * Clase que representa a un nodo del arbol
	 */
	private class Node
	{
		K llave; 
		V val; 
		Node left, right;
		int N;
		boolean color; 

		Node(K key, V val, int N, boolean color)
		{
			this.llave = key;
			this.val = val;
			this.N = N;
			this.color = color;
		}
	}

	private boolean isRed(Node n)
	{
		if (n == null) return false;
		return n.color == RED;
	}

	private Node rotarALaIzquierda(Node k)
	{
		Node x = k.right;
		k.right = x.left;
		x.left = k;
		x.color = k.color;
		k.color = RED;
		x.N = k.N;
		k.N = 1 + size(k.left)
		+ size(k.right);
		return x;
	}

	private Node rotarALaDerecha(Node k)
	{
		Node x = k.left;
		k.left = x.right;
		x.right = k;
		x.color = k.color;
		k.color = RED;
		x.N = k.N;
		k.N = 1 + size(k.left)
		+ size(k.right);
		return x;
	}

	private void flipColors(Node h)
	{
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
	}

	public int size(){
		return size(raiz); 
	}

	private int size(Node x)
	{
		if (x == null) return 0;
		else return x.N;
	}

	public void put(K key, V val)
	{ 
		raiz = put(raiz, key, val);
		raiz.color = BLACK;
	}

	private Node put(Node h, K key, V val)
	{
		if (h == null) 
			return new Node(key, val, 1, RED);
		int cmp = key.compareTo(h.llave);
		if (cmp < 0) h.left = put(h.left, key, val);
		else if (cmp > 0) h.right = put(h.right, key, val);
		else {
			if(val instanceof IQueue){
				concat((Queue)h.val,(Queue)val);
			}
			else {
				h.val = val; 
			} 
		}

		if (isRed(h.right) && !isRed(h.left)) h = rotarALaIzquierda(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotarALaDerecha(h);
		if (isRed(h.left) && isRed(h.right)) flipColors(h);
		h.N = size(h.left) + size(h.right) + 1;
		return h;
	}


	/**
	 * Concatena dos colas
	 * @param q1 cola a la que se quiere concatenar
	 * @param q2 cola que se quiere concatenar
	 */
	public void concat( Queue q1, Queue q2)
	{
		Object el = q2.dequeue();
		while(el != null)
		{
			q1.enqueue(el);
			el = q2.dequeue();
		}

	}



	/**
	 * 
	 */
	public void eliminarMin() throws Exception{
		if (estaVacio()) throw new Exception("No se puede eliminar si no hay nada en el arbol");

		if (!isRed(raiz.left) && !isRed(raiz.right))
			raiz.color = RED;

		raiz = eliminarMin(raiz);
		if (!estaVacio()) raiz.color = BLACK;
	}

	private Node eliminarMin(Node h) { 
		if (h.left == null)
			return null;

		if (!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h);

		h.left = eliminarMin(h.left);
		return invariante(h);
	}


	public boolean estaVacio()
	{
		if(size() == 0) return true;
		else return false;

	}


	/**
	 * Elimina el mayor elemento del arbol.
	 */
	public void eliminarMax() throws Exception {
		if (estaVacio()) throw new Exception("No se puede eliminar si no hay nada en el arbol");

		if (!isRed(raiz.left) && !isRed(raiz.right))
			raiz.color = RED;

		raiz = eliminarMax(raiz);
		if (!estaVacio()) raiz.color = BLACK;
	}


	private Node eliminarMax(Node h) { 
		if (isRed(h.left))
			h = rotarALaDerecha(h);

		if (h.right == null)
			return null;

		if (!isRed(h.right) && !isRed(h.right.left))
			h = moveRedRight(h);

		h.right = eliminarMax(h.right);

		return invariante(h);
	}


	public void eliminar(K key) throws Exception{ 
		if (key == null) throw new IllegalArgumentException("El elemento a eliminar debe ser distinto de null");
		if (get(key)== null) throw new Exception("El elemento a eliminar no existe");

		if (!isRed(raiz.left) && !isRed(raiz.right))
			raiz.color = RED;

		raiz = eliminar(raiz, key);
		if (!estaVacio()) raiz.color = BLACK;
	}

	private Node eliminar(Node h, K key) { 

		if (key.compareTo(h.llave) < 0)  {
			if (!isRed(h.left) && !isRed(h.left.left))
				h = moveRedLeft(h);
			h.left = eliminar(h.left, key);
		}
		else {
			if (isRed(h.left))
				h = rotarALaDerecha(h);
			if (key.compareTo(h.llave) == 0 && (h.right == null))
				return null;
			if (!isRed(h.right) && !isRed(h.right.left))
				h = moveRedRight(h);
			if (key.compareTo(h.llave) == 0) {
				Node x = min(h.right);
				h.llave = x.llave;
				h.val = x.val;
				h.right = eliminarMin(h.right);
			}
			else h.right = eliminar(h.right, key);
		}
		return invariante(h);
	}


	private Node moveRedRight(Node x) {

		flipColors(x);
		if (isRed(x.left.left)) { 
			x = rotarALaDerecha(x);
			flipColors(x);
		}
		return x;
	}



	private Node moveRedLeft(Node h) {

		flipColors(h);
		if (isRed(h.right.left)) { 
			h.right = rotarALaDerecha(h.right);
			h = rotarALaIzquierda(h);
			flipColors(h);
		}
		return h;
	}

	private Node min(Node x) { 
		if (x.left == null) return x; 
		else                return min(x.left); 
	} 

	/**
	 * @return Retorna el valor de la llave dada
	 * @param key la llave
	 * @throws IllegalArgumentException si el par�metro es nulo
	 */
	public V get(K key) {
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
		return get(raiz, key);
	}

	private V get(Node x, K key) {
		while (x != null) {
			int cmp = key.compareTo(x.llave);
			if      (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else              return x.val;
		}
		return null;
	}

	public K select(int k) {
		if (k < 0 || k >= size()) {
			throw new IllegalArgumentException("El �ndice dado no es v�lido" + k);
		}
		Node x = select(raiz, k);
		return x.llave;
	}

	private Node select(Node x, int k) {
		int t = size(x.left); 
		if      (t > k) return select(x.left,  k); 
		else if (t < k) return select(x.right, k-t-1); 
		else            return x; 
	} 

	public K max() throws Exception {
		if (estaVacio()) throw new Exception("calls min() with empty symbol table");
		return max(raiz).llave;
	}

	private Node max(Node x) { 
		if (x.right == null) return x; 
		else                return min(x.right); 
	}



	public int rank(K key) {
		if (key == null) throw new IllegalArgumentException("la llave no puede ser nula");
		return rank(key, raiz);
	} 

	// number of keys less than key in the subtree rooted at x
	private int rank(K key, Node x) {
		if (x == null) return 0; 
		int cmp = key.compareTo(x.llave); 
		if      (cmp < 0) return rank(key, x.left); 
		else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
		else return size(x.left); 
	} 
	public Iterator<K> keys() throws Exception {
		if (estaVacio()) return new Queue<K>().iterator();
		return keysInRange(min(), max());
	}

	public Iterator<K> keysInRange(K lo, K hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<K> queue = new Queue<K>();
		keys(raiz, queue, lo, hi);
		return queue.iterator();
	} 

	
	
	
	private void keys(Node x, Queue<K> queue, K lo, K hi) { 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.llave); 
		int cmphi = hi.compareTo(x.llave); 
		if (cmplo < 0) keys(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.llave); 
		if (cmphi > 0) keys(x.right, queue, lo, hi); 
	} 

	@Override
	public Iterator<V> valuesInRange(K init, K end) {
		if (init == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (end == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<V> queue = new Queue<V>();
		values(raiz, queue, init, end);
		return queue.iterator();
	}

	private void values(Node x, Queue<V> queue, K lo, K hi) { 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.llave); 
		int cmphi = hi.compareTo(x.llave); 
		if (cmplo < 0) values(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.val); 
		if (cmphi > 0) values(x.right, queue, lo, hi); 
	}
	
	public IQueue<V> queueInRange(K lo, K hi)
	{
		IQueue<V> rta = new Queue();
		Iterator<V> hh = valuesInRange(lo, hi);

		while(hh.hasNext())
		{
			rta.enqueue(hh.next());
		}
		return rta;
	}
	
	

	public Iterator<V> values() throws Exception {
		if (estaVacio()) return new Queue<V>().iterator();
		return valuesInRange(min(), max());
	}

	@Override
	public int getHeight(K key) 
	{
		int cont = 0;
		Node nodo = raiz;

		while(nodo != null)
		{

			if(nodo.llave.compareTo(key) == 0)
			{
				return cont;
			}
			else
			{
				if(nodo.llave.compareTo(key) > 0)
				{
					nodo = nodo.left;
				}
				else
				{
					nodo = nodo.right;
				}
				cont++;


			}

		}

		return -1;
	}

	/**
	 * Invariante de la estructura
	 */
	private Node invariante(Node h) {

		if (isRed(h.right))                      h = rotarALaIzquierda(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotarALaDerecha(h);
		if (isRed(h.left) && isRed(h.right))     flipColors(h);

		h.N = size(h.left) + size(h.right) + 1;
		return h;
	}


	public int height() {
		return height(raiz);
	}
	private int height(Node x) {
		if (x == null) return -1;
		return 1 + Math.max(height(x.left), height(x.right));
	}


	public boolean contains(K key) 
	{
		Node nodo = raiz;

		while(nodo != null)
		{
			if(nodo.llave.compareTo(key) == 0)
			{
				return true;
			}else if(nodo.llave.compareTo(key) == 1)
			{
				nodo = nodo.left;
			}else 
			{
				nodo = nodo.right;
			}
		}

		return false;

	}

	@Override
	public K min() throws Exception {
		if (estaVacio()) throw new Exception("calls min() with empty symbol table");
		return min(raiz).llave;
	}

}
