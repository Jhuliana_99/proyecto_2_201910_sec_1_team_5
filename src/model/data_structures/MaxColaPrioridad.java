package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MaxColaPrioridad <T extends Comparable<T>>implements IPriorityQueue{

	
	 private T[] elementos;                   
	 private int n;                       

	
	public MaxColaPrioridad(int capacidad){
		elementos = (T[]) new Object[capacidad + 1];
        n = 0;
	}
	
	 private void resize(int capacity) {
	        assert capacity > n;
	        T[] temp = (T[]) new Object[capacity];
	        for (int i = 1; i <= n; i++) {
	            temp[i] = elementos[i];
	        }
	        elementos = temp;
	    }
	
	@Override
	public int darNumElementos() {
		return n;
	}

	@Override
	public void agregar(Comparable elemento) {
		
		if (n == elementos.length - 1) resize(2 * elementos.length);
		elementos[++n] = (T) elemento;
	    swim(n);
	    assert isMaxHeap();
	    
		
	}

	@Override
	public Comparable delMax() {
		
		if (esVacia()) throw new NoSuchElementException("Priority queue underflow");
        T max = elementos[1];
        exch(1, n--);
        sink(1);
        elementos[n+1] = null;     
        if ((n > 0) && (n == (elementos.length - 1) / 4)) resize(elementos.length / 2);
        assert isMaxHeap();
        return max;
	
		
	}
	
	public Comparable max() {
		
		if (esVacia()) throw new NoSuchElementException("Priority queue underflow");
		return elementos[0];
		
	}

	public boolean less(int i, int j) {
        
        return ((Comparable<T>) elementos[i]).compareTo(elementos[j]) < 0;
        
    }
	

	@Override
	public boolean esVacia() {
		
		if(n == 0)
		{
			return true;
		}
		return false;
	}

	public Iterator<T> iterator() {
        return new HeapIterator();
    }

    private class HeapIterator implements Iterator<T> {

        private MaxColaPrioridad<T> copy;

        public HeapIterator() {
            copy = new MaxColaPrioridad<T>(darNumElementos());

            for (int i = 1; i <= n; i++)
                copy.agregar(elementos[i]);
        }

        public boolean hasNext()  { return !copy.esVacia();                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            return (T) copy.delMax();
        }
    }
	
	private void swim(int k) {
        while (k > 1 && less(k/2, k)) {
            exch(k, k/2);
            k = k/2;
        }
    }
	
	private void sink(int k) {
        while (2*k <= n) {
            int j = 2*k;
            if (j < n && less(j, j+1)) j++;
            if (!less(k, j)) break;
            exch(k, j);
            k = j;
        }
    }
	
	 private void exch(int i, int j) {
	        T swap = elementos[i];
	        elementos[i] = elementos[j];
	        elementos[j] = swap;
	    }
	 
	 public boolean isMaxHeap() {
	        return isMaxHeap(1);
	    }
	 
	 private boolean isMaxHeap(int k) {
	        if (k > n) return true;
	        int left = 2*k;
	        int right = 2*k + 1;
	        if (left  <= n && less(k, left))  return false;
	        if (right <= n && less(k, right)) return false;
	        return isMaxHeap(left) && isMaxHeap(right);
	    }

	


	

}
