package model.data_structures;

import java.util.Iterator;

public interface IBSTRojoNegro<K extends Comparable<K>, V>{

	/**
	 * @return Retorna el tama�o del arbol
	 */
	public int size(); 

	/**
	 * Indica si el arbol est� vacio
	 * @return retorna true en caso de que est� vacio, false en caso contrario
	 */
	public boolean estaVacio();
	
	/**
	 * Retorna el valor asociado a la llave. 
	 * @return El valor.
	 */
	public V get(K key);
	
	/**
	 * Retorna la altura hasta la llave.
	 * @return La altura del arbol hasta la llave
	 * @param key, La llave que se va a buscar
	 */
	public int getHeight(K key);
	
	/**
	 * Indica si la llave se encuentra en el arbol.
	 * @param key La llave que se va a buscar.
	 * @return Retorna true si se encontro o false si no se encontro.
	 */
	public boolean contains(K key);

	/**
	 * Agrega un elemento al arbol, el arbol est� inicializado
	 * @param key, la llave del elemento
	 * @param val, el valor del elemento
	 */
	public void put(K key, V val);

	/**
	 * Retorna la altura de la rama mas alta
	 * @return La altura de la rama mas alta.
	 */
	public int height();
	
	/**
	 * Retorna la llave menor del arbol.
	 * @return La llave menor del arbol.
	 * @throws Exception 
	 */
	public K min() throws Exception;
	
	/**
	 * Retorna la llave mayor del arbol.
	 * @return La llave mayor del arbol.
	 * @throws Exception 
	 */
	public K max() throws Exception;

	
	/**
	 * Retorna todas las llaves del arbol.
	 * @return el Iterador de las llaves del arbol.
	 * @throws Exception 
	 */
	public Iterator<K> keys() throws Exception;
	
	/**
	 * Retorna los valores del arbol que esten en el rango.
	 * @return los valores del arbol que estan en el rango.
	 */
	public Iterator<V> valuesInRange(K init, K end);
	
	/**
	 * Retorna las llaves del arbol que esten en el rango.
	 * @return Las llaves del arbol que estan en el rango.
	 */
	public Iterator<K> keysInRange(K init, K end);
	
	/**
	 * Elimina el elemento m�s peque�o del arbol
	 * @throws Exception, si el arbol est� vacio.
	 */
	public void eliminarMin() throws Exception;

	/**
	 * Elimina el elemento m�s grande del arbol-
	 * @throws Exception, si el arbol est� vacio
	 */
	public void eliminarMax() throws Exception;

	/**
	 * Elimina la llave espec�ficada y su valor del arbol, siempre que la llave est� en el arbol
	 * @param  key, la llave
	 * @throws Exception si el elemento a eliminar no existe
	 * @throws IllegalArgumentException si se intenta eliminar una instancia nula
	 */
	public void eliminar(K key)throws Exception;

	public Iterator<V> values() throws Exception;
	
	public IQueue queueInRange(K lo, K hi);
}
