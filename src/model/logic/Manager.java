package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Iterator;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

import model.data_structures.IQueue;
import model.data_structures.Queue;
import comparators.ByNumOfInfr;
import model.data_structures.BSTRojoNegro;
import model.data_structures.IBSTRojoNegro;
import model.data_structures.IHeap;
import model.data_structures.IQueue;
import model.data_structures.ITablaHash;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHash;
import model.vo.EstadisticaInfracciones;
import model.vo.EstadisticasCargaInfracciones;
import model.vo.InfraccionesFecha;
import model.vo.InfraccionesFechaHora;
import model.vo.InfraccionesFranjaHoraria;
import model.vo.InfraccionesFranjaHorariaViolationCode;
import model.vo.InfraccionesLocalizacion;
import model.vo.InfraccionesViolationCode;
import model.vo.VOMovingViolations;
import model.vo.dupla;
import view.MovingViolationsManagerView;

public class Manager {

	private IBSTRojoNegro<String, VOMovingViolations> BST;
	private ITablaHash<String, VOMovingViolations> tablita;
	private ITablaHash<String,IQueue<VOMovingViolations>> hashPorHora;
	private ITablaHash<Integer,InfraccionesLocalizacion> hashPorAddress;
	private ITablaHash<dupla<Double, Double>,InfraccionesLocalizacion> hashPorLoc;
	private IBSTRojoNegro<String, IQueue<VOMovingViolations>> arbolitoPorFecha;
	
	/**
	 * Constructor
	 */
	public Manager() {
		hashPorHora = new SeparateChainingHash<String,IQueue<VOMovingViolations>>(24);
		hashPorAddress = new SeparateChainingHash<Integer,InfraccionesLocalizacion>(4977);
		hashPorLoc = new SeparateChainingHash<dupla<Double, Double>,InfraccionesLocalizacion>(50000);
		arbolitoPorFecha = new BSTRojoNegro<String, IQueue<VOMovingViolations>>();
		BST = new BSTRojoNegro<String, VOMovingViolations>();
		tablita = new SeparateChainingHash<String, VOMovingViolations>(670000) ;
	}


	

	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	public int loadMovingViolations(int numeroSemestre) {
		int numDatos=0;
		String pth;
		int ini; int fin;
		String rta= "";
		int k = 0;


		if(numeroSemestre == 1)
		{
			ini = 1; fin = 6;
		}
		else if(numeroSemestre != 1){
			ini = 7; fin = 12;
		}
		else{
			System.out.println("El semestre no tiene un formato v�lido, por favor ingrese 1 o 2"); ini =0; fin = -1;
		}

		for(int j=ini; j<= fin; j++){

			pth = "./data/Moving_Violations_Issued_in_"+ Integer.toString(j)+"_2018.csv";

			int cont= 0;

			Path myPath = Paths.get(pth);
			try (BufferedReader br = Files.newBufferedReader(myPath)){


				HeaderColumnNameMappingStrategy<VOMovingViolations> strategy = new HeaderColumnNameMappingStrategy<>();
				strategy.setType(VOMovingViolations.class);
				CsvToBean csvToB = new CsvToBeanBuilder(br).withType(VOMovingViolations.class).withMappingStrategy(strategy).withIgnoreLeadingWhiteSpace(true).build();

				for(Iterator<VOMovingViolations> i = csvToB.parse().iterator();i.hasNext(); ){

					VOMovingViolations g = i.next();
					tablita.put(g.getobjectid(), g);
					BST.put(g.getobjectid(), g);
					IQueue<VOMovingViolations> listita = new Queue<VOMovingViolations>();
					
					//Agrega a hash por localizaci�n
					listita.enqueue(g);
					String loc = g.getLocation();
					int strt = g.getStreetsegid();
					Double x = g.getXcoord();
					Double y = g.getYcoord();
					int address = g.getAddress_id();
					InfraccionesLocalizacion lf = new InfraccionesLocalizacion(x,y, loc,address,strt,listita);
					dupla<Double, Double> dpla = new dupla<Double,Double>(x,y);
					hashPorLoc.put(dpla, lf);
					
					//Agrega a hash por hora
					listita.enqueue(g);
					String hour = g.getTicketIssueDate().substring(11,13);
					hashPorHora.put(hour, listita);

					//Agrega a arbol por fecha
					listita.enqueue(g);
					InfraccionesLocalizacion gg = new InfraccionesLocalizacion(x,y, loc,address,strt,listita);
					hashPorAddress.put(address, gg);

					//Agrega a arbol por fecha
					listita.enqueue(g);
					String fecha = g.getTicketIssueDate().substring(0,10);
					arbolitoPorFecha.put(fecha, listita);
				}

				rta = rta + "El n�mero de infracciones en el mes n�mero " + Integer.toString(j) + " es de " + Integer.toString(cont) + "\n";
				cont =0;
			}

			catch (FileNotFoundException e1) {

			} 
			catch (IOException e) {

			}
		}

		return (int)hashPorLoc.getAux() ;
	}


	/**
	 * Requerimiento 1A: Obtener el ranking de las N franjas horarias
	 * que tengan m�s infracciones. 
	 * @param int N: N�mero de franjas horarias que tienen m�s infracciones
	 * @return Cola con objetos InfraccionesFranjaHoraria
	 */
	public IQueue<InfraccionesFranjaHoraria> rankingNFranjas(int N)
	{
		IHeap<EstadisticaInfracciones> heap = new MaxHeapCP<EstadisticaInfracciones>(24, new ByNumOfInfr());
		Iterator<String> iter = hashPorHora.keys();

		while(iter.hasNext())
		{
			
			String elemento = iter.next();
			InfraccionesFranjaHoraria dato = new InfraccionesFranjaHoraria(ManejoFechaHora.convertirHora_LT(elemento + ":00:00"),ManejoFechaHora.convertirHora_LT(elemento + ":59:59"),hashPorHora.get(elemento));
			heap.agregar(dato);
		}

		IQueue<InfraccionesFranjaHoraria> rta = new Queue<InfraccionesFranjaHoraria>();

		for(int i =0; i < N; i++)
			rta.enqueue((InfraccionesFranjaHoraria) heap.delMax());

		return rta;
	}

	/**
	 * Requerimiento 2A: Consultar  las  infracciones  por
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Tabla Hash.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionHash(double xCoord, double yCoord)
	{ 
		InfraccionesLocalizacion infr = (InfraccionesLocalizacion) hashPorLoc.get(new dupla(xCoord,yCoord));

		return infr;	
	}

	/**
	 * Requerimiento 3A: Buscar las infracciones por rango de fechas
	 * @param  LocalDate fechaInicial: Fecha inicial del rango de b�squeda
	 * 		LocalDate fechaFinal: Fecha final del rango de b�squeda
	 * @return Cola con objetos InfraccionesFecha
	 */
	public IQueue<InfraccionesFecha> consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal)
	{		
		IQueue<InfraccionesFecha> rta = new Queue<InfraccionesFecha>();

		Iterator<String> iterador = arbolitoPorFecha.keysInRange(fechaInicial.toString(), fechaFinal.toString());
		while(iterador.hasNext())
		{
			String fecha = iterador.next();
			IQueue<VOMovingViolations> cola = arbolitoPorFecha.get(fecha);
			LocalDate fechaconvertida = LocalDate.parse(fecha);
			InfraccionesFecha dato = new InfraccionesFecha(cola,fechaconvertida);

			rta.enqueue(dato);
		}

		return rta;
	}

	/**
	 * Requerimiento 1B: Obtener  el  ranking  de  las  N  tipos  de  infracci�n
	 * (ViolationCode)  que  tengan  m�s infracciones.
	 * @param  int N: Numero de los tipos de ViolationCode con m�s infracciones.
	 * @return Cola con objetos InfraccionesViolationCode con top N infracciones
	 */
	public IQueue<InfraccionesViolationCode> rankingNViolationCodes(int N) throws Exception
	{
		
		IQueue<InfraccionesViolationCode> cola = new Queue<InfraccionesViolationCode>();
		
		Iterator<VOMovingViolations> iterador = BST.values();
		
		VOMovingViolations infraccion = null;
		
		IQueue<VOMovingViolations> infracciones = new Queue<VOMovingViolations>(); 

		SeparateChainingHash<String, IQueue<VOMovingViolations>> tabla = new SeparateChainingHash<String, IQueue<VOMovingViolations>>(BST.size());
		
		while(iterador.hasNext())
		{
			infraccion = iterador.next();
			
			infracciones.enqueue(infraccion);
			
			tabla.put(infraccion.getViolationcode(), infracciones);
			
		}
		
		Iterator<String> iter = tabla.keys();
		InfraccionesViolationCode v = null;
		MaxColaPrioridad<EstadisticaInfracciones> colaP = new MaxColaPrioridad<EstadisticaInfracciones>(tabla.getAux());
		
		while(iterador.hasNext())
		{
			String key = iter.next();
			
			infracciones = tabla.get(key);
			
			v = new InfraccionesViolationCode(key, infracciones);
			
			colaP.agregar(v);
			
		}
		
		for(int i =0; i < colaP.darNumElementos(); i++)
			cola.enqueue((InfraccionesViolationCode) colaP.delMax());
		
		return cola;		
	}


	/**
	 * Requerimiento 2B: Consultar las  infracciones  por  
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Arbol.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionArbol(double xCoord, double yCoord) throws Exception
	{
		
		IBSTRojoNegro<String, VOMovingViolations> arbol = organizarPorLocalizacionArbol();
		
		IQueue<VOMovingViolations> cola = null;
		
		Iterator<VOMovingViolations> iterador = arbol.valuesInRange(""+xCoord, ""+xCoord);
		
		VOMovingViolations ejemplo = null;
		
		while(iterador.hasNext())
		{
			VOMovingViolations infraccion = iterador.next();
			
			if(infraccion.getYcoord().equals(yCoord)){
				cola.enqueue(infraccion);
				ejemplo = infraccion;
			}
		}
		
		InfraccionesLocalizacion rta = new InfraccionesLocalizacion(xCoord, yCoord, ejemplo.getLocation(), ejemplo.getAddress_id(),ejemplo.getStreetsegid(), cola);
		
		
		return rta;		
	}
	
	private IBSTRojoNegro<String, VOMovingViolations> organizarPorLocalizacionArbol() throws Exception
	{
		IBSTRojoNegro<String, VOMovingViolations> arbol = new BSTRojoNegro<String, VOMovingViolations>();
		Iterator<VOMovingViolations> iterador = BST.values();
		
		while(iterador.hasNext())
		{
			VOMovingViolations infraccion = iterador.next();
			
			arbol.put(""+infraccion.getXcoord(), infraccion);
		}
		
		return arbol;
	}

	/**
	 * Requerimiento 3B: Buscar las franjas de fecha-hora donde se tiene un valor acumulado
	 * de infracciones en un rango dado [US$ valor inicial, US$ valor final]. 
	 * @param  double valorInicial: Valor m�nimo acumulado de las infracciones
	 * 		double valorFinal: Valor m�ximo acumulado de las infracciones.
	 * @return Cola con objetos InfraccionesFechaHora
	 */
	public IQueue<InfraccionesFechaHora> consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal) throws Exception
	{
		Iterator<VOMovingViolations> iterador = BST.values();
		
		VOMovingViolations infraccion = infraccion = iterador.next();;
		
		IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
		
		BSTRojoNegro<Double, InfraccionesFechaHora> arbol = new BSTRojoNegro<Double, InfraccionesFechaHora>();

		
		while(iterador.hasNext())
		{
			
			String fecha = infraccion.getTicketIssueDate().substring(0,10);
			
			int cont = 0;
			
			while(infraccion != null && infraccion.getTicketIssueDate().substring(0,10).equals(fecha))
			{
				String hora = "0" + cont;
				hora = hora.substring(hora.length()-1, hora.length());
				if(infraccion.getTicketIssueDate().substring(12, 13).equals((hora)))
				{
					cola.enqueue(infraccion);
					infraccion = iterador.next();
				}else
				{
					InfraccionesFechaHora v = new InfraccionesFechaHora(fecha + "T"+hora+":00", fecha + "T"+hora+":59", cola);
					arbol.put(v.getValorTotal(), v);
					cont++;
					
				}
				
			}
			
			
		}
		
		Iterator<InfraccionesFechaHora> iter  = arbol.valuesInRange(valorInicial, valorFinal);
		
		InfraccionesFechaHora infracciones = null;
		
		IQueue<InfraccionesFechaHora> colaFinal = new Queue<InfraccionesFechaHora>();
		
		while(iter.hasNext())
		{
			infracciones = iter.next();
			
			colaFinal.enqueue(infracciones);
		}
		return colaFinal;		
	}

	/**
	 * Requerimiento 1C: Obtener  la informaci�n de  una  addressId dada
	 * @param  int addressID: Localizaci�n de la consulta.
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorAddressId(int addressID)
	{
		InfraccionesLocalizacion rta = (InfraccionesLocalizacion) hashPorAddress.get(addressID);

		return rta;	
	}

	/**
	 * Requerimiento 2C: Obtener  las infracciones  en  un  rango de
	 * horas  [HH:MM:SS  inicial,HH:MM:SS  final]
	 * @param  LocalTime horaInicial: Hora  inicial del rango de b�squeda
	 * 		LocalTime horaFinal: Hora final del rango de b�squeda
	 * @return Objeto InfraccionesFranjaHorariaViolationCode
	 */
	public InfraccionesFranjaHorariaViolationCode consultarPorRangoHoras(LocalTime horaInicial, LocalTime horaFinal) throws Exception
	{
		Iterator<VOMovingViolations> iterador = BST.values();
		
		
		IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
		
		VOMovingViolations infraccion = null;
		while(iterador.hasNext())
		{
			
			infraccion = iterador.next();

			
			LocalTime hora = ManejoFechaHora.convertirHora_LT(infraccion.getTicketIssueDate().substring(11,19));
			
			

			
			if((horaInicial.compareTo(hora)==-1 && horaFinal.compareTo(hora)==1) || horaInicial.compareTo(hora)==0 || horaFinal.compareTo(hora)==0 )
			{
				cola.enqueue(infraccion);
			}
			
		}
				
		iterador = cola.iterator();
		
		
		
		IQueue<InfraccionesViolationCode> cola2 = new Queue<InfraccionesViolationCode>();
		
		
		SeparateChainingHash<String, IQueue<VOMovingViolations>> tabla = new SeparateChainingHash<String, IQueue<VOMovingViolations>>(cola.size());
		
		while(iterador.hasNext())
		{

			infraccion = iterador.next();
			
			IQueue<VOMovingViolations> infracciones = new Queue<VOMovingViolations>();
			
			infracciones.enqueue(infraccion);
			
			tabla.put(infraccion.getViolationcode(), infracciones);
			
			
		}
		
		Iterator<String> iter = tabla.keys();
	
		String key = null;
		
		while(iter.hasNext())
		{
			key = iter.next();
			
			InfraccionesViolationCode z = new InfraccionesViolationCode(key, tabla.get(key));
			cola2.enqueue(z);
		}
		
		
		InfraccionesFranjaHorariaViolationCode x = new InfraccionesFranjaHorariaViolationCode(horaInicial, horaFinal, cola, cola2);

		return x;		
	}

	/**
	 * Requerimiento 3C: Obtener  el  ranking  de  las  N localizaciones geogr�ficas
	 * (Xcoord,  Ycoord)  con  la mayor  cantidad  de  infracciones.
	 * @param  int N: Numero de las localizaciones con mayor n�mero de infracciones
	 * @return Cola de objetos InfraccionesLocalizacion
	 */
	public IQueue<InfraccionesLocalizacion> rankingNLocalizaciones(int N)
	{
		IHeap<EstadisticaInfracciones> heap = new MaxHeapCP<EstadisticaInfracciones>(5000, new ByNumOfInfr());
		Iterator<dupla<Double, Double>> iter = hashPorLoc.keys();

		while(iter.hasNext())
		{
			dupla<Double, Double> elemento = iter.next();
			InfraccionesLocalizacion dato = hashPorLoc.get(elemento);

			heap.agregar(dato);
		}

		IQueue<InfraccionesLocalizacion> rta = new Queue<InfraccionesLocalizacion>();

		for(int i =0; i < N; i++)
			rta.enqueue((InfraccionesLocalizacion) heap.delMax());

		return rta;	
	}
	
	/**
	  * Requerimiento 4C: Obtener la  informaci�n  de  los c�digos (ViolationCode) ordenados por su numero de infracciones.
	  * @return Contenedora de objetos InfraccionesViolationCode.
	  // TODO Definir la estructura Contenedora
	 * @throws Exception 
	  */
	public IQueue<InfraccionesViolationCode> ordenarCodigosPorNumeroInfracciones() throws Exception
	{
		Iterator<VOMovingViolations> iterador = BST.values();
		
		VOMovingViolations infraccion = null;
						
		BSTRojoNegro<String, IQueue<VOMovingViolations>> arb = new BSTRojoNegro<String, IQueue<VOMovingViolations>>();
		
		while(iterador.hasNext())
		{
			
			infraccion = iterador.next();
			
			IQueue<VOMovingViolations> infracciones = new Queue<VOMovingViolations>();
			
			infracciones.enqueue(infraccion);
			
			arb.put(infraccion.getViolationcode(), infracciones);		
			
		}
		
		

		
		Iterator<String> iter = arb.keys();
		
		String key = "";
		
		BSTRojoNegro<Integer, InfraccionesViolationCode> arbol = new BSTRojoNegro<Integer, InfraccionesViolationCode>(); 
		
		while(iter.hasNext())
		{
			key = iter.next();
			
			InfraccionesViolationCode v = new InfraccionesViolationCode(key, arb.get(key));
			
			arbol.put(v.getTotalInfracciones(), v);
			

		}
		

		
		Iterator<InfraccionesViolationCode> iter2 = arbol.values();
		
		IQueue<InfraccionesViolationCode> cola = new Queue<InfraccionesViolationCode>();
		
		InfraccionesViolationCode x = null;
		
		while(iter2.hasNext())
		{
			x = iter2.next();
			
			cola.enqueue(x);
			

		}
		
		return cola;		
	}


}
